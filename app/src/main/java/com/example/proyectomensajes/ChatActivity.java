package com.example.proyectomensajes;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_layout);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.chat_layout, null);

        ScrollView sv = v.findViewById(R.id.svMensajes);

        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams paramsR = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        paramsR.gravity = Gravity.RIGHT;
        LinearLayout.LayoutParams paramsL = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        paramsL.gravity = Gravity.LEFT;

        for (int i = 1; i<=100;i++){
            TextView tv = new TextView(this);
            tv.setText("my text "+i);

            if(i%2==0) {
                tv.setLayoutParams(paramsR);
                tv.setBackgroundColor(Color.parseColor("#E2FFC9"));
            } else {
                tv.setLayoutParams(paramsL);
                tv.setBackgroundColor(Color.parseColor("#FCFFFFTe"));
            }
            ll.addView(tv);
        }
        sv.addView(ll);

        setContentView(v);
    }
}