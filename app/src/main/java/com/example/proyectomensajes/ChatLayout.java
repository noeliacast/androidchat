package com.example.proyectomensajes;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ChatLayout extends Activity {

    ArrayList<Mensaje> mensajes;
    int idUsuario;
    int idGrupo;
    LinearLayout ll;
    ScrollView listMensajes;
    View v;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_list);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.chat_layout, null);

        idUsuario = getIntent().getIntExtra("idUser", 0);
        idGrupo = getIntent().getIntExtra("idGrupo", 0);

        listMensajes = v.findViewById(R.id.svMensajes);

        ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);

        listMensajes.addView(ll);
        setContentView(v);

        Button btnEnviar = findViewById(R.id.btnEnviar);
        btnEnviar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                TextView txtMensaje = findViewById(R.id.editMensaje);
                String mensaje = txtMensaje.getText().toString().trim();

                if (!mensaje.equals(""))
                {
                    RequestEnviarMensaje r = new RequestEnviarMensaje(idGrupo, idUsuario, mensaje, null);
                    RequestGetMensajes r2 = new RequestGetMensajes(idGrupo,idUsuario, null);
                    RequestQueue cola = Volley.newRequestQueue(getApplicationContext());
                    cola.add(r);  // Aquí se ejectua.
                    cola.add(r2);  // Aquí se ejectua.
                }
            }
        });

        getMensajes();
        Runnable runnable = new Hiloso(this);
        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void getMensajes()
    {
        mensajes = new ArrayList<>();
        Response.Listener<String> respuesta = new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                JsonArray jsonArray = new JsonParser().parse(response).getAsJsonArray();

                if (!jsonArray.isJsonNull())
                {
                    for (JsonElement jsonElement : jsonArray)
                    {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
                        Timestamp timestamp = null;
                        JsonObject jsonObject = jsonElement.getAsJsonObject();
                        Mensaje mensaje = new Mensaje();

                        try {
                            Date parsedDate = dateFormat.parse(jsonObject.get("fechaenv").getAsString());
                            timestamp = new java.sql.Timestamp(parsedDate.getTime());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        mensaje.setRemitenteId(jsonObject.get("IdRemitente").getAsInt());
                        mensaje.setRemitenteNombre(jsonObject.get("Remitente").getAsString());
                        mensaje.setMensaje(jsonObject.get("Mensaje").getAsString());
                        mensaje.setFecha(timestamp);

                        mensajes.add(mensaje);
                    }
                    LinearLayout.LayoutParams paramsL = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    LinearLayout.LayoutParams paramsR = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    paramsL.gravity = Gravity.LEFT;
                    paramsR.gravity = Gravity.RIGHT;
                    for (Mensaje mensaje : mensajes)
                    {
                        TextView label = new TextView(getApplicationContext());

                        label.setText(mensaje.getMensaje());
                        ll.addView(label);


                        if (mensaje.getRemitenteId() == idUsuario)
                        {
                            label.setLayoutParams(paramsL);
                        }
                        else
                        {
                            label.setLayoutParams(paramsR);
                        }
                    }
                }
            }
        };

        RequestGetMensajes r = new RequestGetMensajes(idGrupo, idUsuario, respuesta);
        RequestQueue cola = Volley.newRequestQueue(this);
        cola.add(r);  // Aquí se ejectua.
    }
}