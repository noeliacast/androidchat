package com.example.proyectomensajes;

import android.app.Activity;
import android.os.Bundle;

import java.sql.Timestamp;

class Mensajes extends Activity {

    int grupo;
    int remitente;
    String mensaje;
    Timestamp fecha;

    public Mensajes(int grupo, int remitente, String mensaje, Timestamp fecha) {
        this.grupo = grupo;
        this.remitente = remitente;
        this.mensaje = mensaje;
        this.fecha = fecha;
    }
}
