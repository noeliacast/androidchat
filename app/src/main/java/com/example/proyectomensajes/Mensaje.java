package com.example.proyectomensajes;

import android.app.Activity;
import android.os.Bundle;

import java.sql.Timestamp;

class Mensaje extends Activity {

    int grupo;
    int remitenteId;
    String remitenteNombre;
    String mensaje;
    Timestamp fecha;

    public Mensaje(int grupo, int remitenteId, String mensaje, Timestamp fecha, String remitenteNombre)
    {
        this.grupo = grupo;
        this.remitenteId = remitenteId;
        this.mensaje = mensaje;
        this.fecha = fecha;
        this.remitenteNombre = remitenteNombre;
    }

    public Mensaje()
    {
        this.grupo = grupo;
        this.remitenteId = remitenteId;
        this.mensaje = mensaje;
        this.fecha = fecha;
        this.remitenteNombre = remitenteNombre;
    }

    public int getGrupo()
    {
        return grupo;
    }

    public void setGrupo(int grupo)
    {
        this.grupo = grupo;
    }

    public int getRemitenteId()
    {
        return remitenteId;
    }

    public void setRemitenteId(int remitenteId)
    {
        this.remitenteId = remitenteId;
    }

    public String getRemitenteNombre()
    {
        return remitenteNombre;
    }

    public void setRemitenteNombre(String remitenteNombre)
    {
        this.remitenteNombre = remitenteNombre;
    }

    public String getMensaje()
    {
        return mensaje;
    }

    public void setMensaje(String mensaje)
    {
        this.mensaje = mensaje;
    }

    public Timestamp getFecha()
    {
        return fecha;
    }

    public void setFecha(Timestamp fecha)
    {
        this.fecha = fecha;
    }
}
