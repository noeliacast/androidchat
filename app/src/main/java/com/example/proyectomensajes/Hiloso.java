package com.example.proyectomensajes;


public class Hiloso implements Runnable {

    ChatLayout chatLayout;

    public Hiloso(ChatLayout chatLayout)
    {
        this.chatLayout = chatLayout;
    }

    @Override
    public void run()
    {
        while (true)
        {
            chatLayout.getMensajes();
            try {
                Thread.sleep(600);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
