package com.example.proyectomensajes;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class RequestRegistrar extends StringRequest {

    private static final String ruta = "http://192.168.3.81/AndroidChat/put_usuario.php"; // "http://10.1.131.139/niko/put_usuario.php";
    private Map<String, String> parametros;

    public RequestRegistrar(String usuario, String pass, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Request.Method.POST, ruta, listener, errorListener);

        parametros = new HashMap<>();
        parametros.put("nombre", usuario);
        parametros.put("pass", pass);
    }

    @Override
    protected Map<String, String> getParams() {
        return parametros;
    }
}
