package com.example.proyectomensajes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import static android.widget.Toast.LENGTH_LONG;


public class MainActivity extends AppCompatActivity {

    EditText txtLogUser, txtLogPass;
    Button btnLog, btnSign, btnCancel;
    SQLiteDatabase db;
    MediaPlayer loginCorrect, loginWrong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtLogUser = findViewById(R.id.txtLogUser);
        txtLogPass = findViewById(R.id.txtLogPass);
        btnLog = findViewById(R.id.btnLog);
        btnSign = findViewById(R.id.btnSignIn);
        btnCancel = findViewById(R.id.btnLogCancel);
        loginCorrect = MediaPlayer.create(this, R.raw.correct);
        loginWrong = MediaPlayer.create(this, R.raw.wrong);

        BBDD database = new BBDD(this, "AndroidChat", null, 1);
        db = database.getReadableDatabase();

        btnLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtLogUser.getText().length() > 0 && txtLogPass.getText().length() > 0) {
                    logIn();
                } else {
                    Toast.makeText(getApplicationContext(), "ERROR. Los campos de usuario y contraseña tienen que estar llenos.", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnSign.getText().toString().equals(getString(R.string.btn_sign_in))) {
                    btnLog.setVisibility(View.GONE);
                    btnSign.setText(getString(R.string.btn_new_sign_in));
                    btnCancel.setVisibility(View.VISIBLE);
                } else if (btnSign.getText().toString().equals(getString(R.string.btn_new_sign_in))) {
                    registrarUsuario();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnSign.getText().toString().equals(getString(R.string.btn_new_sign_in))) {
                    btnSign.setText(getString(R.string.btn_sign_in));
                    btnCancel.setVisibility(View.GONE);
                    btnLog.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void registrarUsuario() {
        Response.Listener<String> respuesta = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonRepuesta = new JSONObject(response);
                    boolean ok = jsonRepuesta.getBoolean("success"); // El php nos responde un array con un valor "success". Aquó lo rescatamos- Es un  boolea
                    if (ok) {
                        int id = jsonRepuesta.getInt("id");
                        try{
                            String sql = "INSERT INTO Usuarios (Id, Nombre_Usuario, Password) VALUES ("+id+",'" + txtLogUser.getText().toString() + "','" + txtLogPass.getText().toString() + "');";
                           // db.execSQL("INSERT INTO Usuarios (Id, Nombre_Usuario, Password) VALUES ("+id+",'" + txtLogUser.getText().toString() + "','" + txtLogPass.getText().toString() + "');");
                            db.execSQL(sql);
                        }catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(), "Error insercion local", LENGTH_LONG).show();
                        }
                        Toast.makeText(getApplicationContext(), "Inserción en la BBDD Correcta.", LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), EscogerImagen.class);
                        intent.putExtra("idUser", id);
                        startActivity(intent);
                        btnCancel.performClick();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Error registro SQLite", LENGTH_LONG).show();
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        };

        RequestRegistrar r = new RequestRegistrar(txtLogUser.getText().toString(), txtLogPass.getText().toString(), respuesta, errorListener);
        RequestQueue cola = Volley.newRequestQueue(this);
        cola.add(r);  // Aquí se ejectua.
    }

    private void logIn() {

        Response.Listener<String> respuesta = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();
                JsonElement je = jsonObject.get("id");
                if (!je.isJsonNull()) {
                    int id = je.getAsInt();
                    loginCorrect.start();
                    Toast.makeText(getApplicationContext(), "Log in correcto. Verificalo con el PhpAdmin.", LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), GruposListActivity.class);
                    intent.putExtra("idUser", id);
                    startActivity(intent);
                } else
                    Toast.makeText(getApplicationContext(), "LOGIN INCORRECTO", LENGTH_LONG).show();
                    loginWrong.start();
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Cursor c = db.rawQuery("SELECT Id FROM Usuarios WHERE Nombre_Usuario LIKE '" + txtLogUser.getText().toString() + "' AND PASSWORD LIKE '" + txtLogPass.getText().toString() + "';", null);
                int id = 0;
                if (c.getCount() > 0) {
                    try {
                        if (c.moveToFirst()) {
                            id = c.getInt(0);
                        }
                    } finally {
                        c.close();
                    }
                    Intent intent = new Intent(getApplicationContext(), GruposListActivity.class);
                    intent.putExtra("idUser", id);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "LOGIN INCORRECTO", LENGTH_LONG).show();
                }
            }
        };
        RequestLogin r = new RequestLogin(txtLogUser.getText().toString(), txtLogPass.getText().toString(), respuesta, errorListener);
        RequestQueue cola = Volley.newRequestQueue(this);
        cola.add(r);  // Aquí se ejectua.
    }
}
