package com.example.proyectomensajes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class EscogerImagen extends AppCompatActivity {

    ImageView icon1, icon2, icon3, icon4;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.escoger_imagen);

        icon1 = findViewById(R.id.icon1);
        icon2 = findViewById(R.id.icon2);
        icon3 = findViewById(R.id.icon3);
        icon4 = findViewById(R.id.icon4);

        BBDD database = new BBDD(this, "AndroidChat", null, 1);
        db = database.getWritableDatabase();

        final int idUser = getIntent().getIntExtra("idUser", 0);

        icon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.execSQL("UPDATE Usuarios SET FotoPerfil = 1 WHERE Id =" + idUser);
                //llamar a php para actualizar la otra asquerosa
            }
        });

    }
}
