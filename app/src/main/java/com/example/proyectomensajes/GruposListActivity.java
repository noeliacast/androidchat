package com.example.proyectomensajes;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import static android.widget.Toast.LENGTH_LONG;

public class GruposListActivity extends Activity {

    int idUser;
    ListView listGrupos;
    ArrayList<Grupo> grupos = new ArrayList<Grupo>();
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_list);
        BBDD database = new BBDD(this, "AndroidChat", null, 1);
        db = database.getReadableDatabase();

        idUser = getIntent().getIntExtra("idUser", 0);

        listGrupos = findViewById(R.id.listV);

        getGrupos();

        ContactList adaptador = new ContactList(this);
        listGrupos.setAdapter(adaptador);

        listGrupos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), ChatLayout.class);
                intent.putExtra("idUser", idUser);
                intent.putExtra("idGrupo", grupos.get(i).getId());
                startActivity(intent);
            }
        });
    }

    private void getGrupos() {
        Response.Listener<String> respuesta = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JsonArray jsonArray = new JsonParser().parse(response).getAsJsonArray();
                    if (!jsonArray.isJsonNull()) {
                        for (JsonElement jsonElement : jsonArray) {
                            JsonObject jsonObject = jsonElement.getAsJsonObject();
                            int id = jsonObject.get("IdGrupo").getAsInt();
                            String nombreGrupo = jsonObject.get("NombreGrupo").getAsString();
                            Grupo g = new Grupo(id, nombreGrupo);
                            grupos.add(g);
                        }
                        listGrupos.invalidateViews();
                    } else {
                        Toast.makeText(getApplicationContext(), "ERROR INESPERADO", LENGTH_LONG).show();
                    }
                } catch (IllegalStateException e) {
                }
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Cursor c = db.rawQuery("SELECT Grupos.Id, Grupos.Nombre_Grupo FROM Grupos INNER JOIN Grupos_Usuarios ON Grupos.Id = Grupos_Usuarios.Grupo WHERE Grupos_Usuarios.Usuario ="+idUser, null);

                if(c!=null && c.moveToFirst()){
                    do{
                        int id = c.getInt(0);
                        String nombreGrupo = c.getString(1);
                        Grupo g = new Grupo(id, nombreGrupo);
                        grupos.add(g);
                    } while(c.moveToNext());
                    listGrupos.invalidateViews();
                } else{
                    Toast.makeText(getApplicationContext(), "ERROR INESPERADO", LENGTH_LONG).show();
                }
            }
        };

        RequestGetGruposUsuario r = new RequestGetGruposUsuario(idUser, respuesta, errorListener);
        RequestQueue cola = Volley.newRequestQueue(this);
        cola.add(r);  // Aquí se ejectua.
    }

    class ContactList extends ArrayAdapter {

        Activity context;

        public ContactList(Activity context) {
            super(context, R.layout.list_element, grupos);
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.list_element, null);

            TextView lblNomGrup = item.findViewById(R.id.txtGroupName);

            lblNomGrup.setText(grupos.get(position).getNombreGurpo());

            return item;
        }
    }

}
