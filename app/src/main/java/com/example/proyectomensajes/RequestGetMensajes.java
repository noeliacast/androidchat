package com.example.proyectomensajes;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class RequestGetMensajes extends StringRequest {
    private static String ruta = "http://192.168.3.81/AndroidChat/getMensajes.php";
    private Map<String, String> parametros;

    public RequestGetMensajes(Integer idGrupo,Integer idUsuario, Response.Listener<String> listener)
    {
        super(Method.POST, ruta, listener, null);
        parametros = new HashMap<>();
        parametros.put("grupo", idGrupo.toString());
        parametros.put("user", idUsuario.toString());
    }

    protected Map<String, String> getParams() {
        return parametros;
    }
}
