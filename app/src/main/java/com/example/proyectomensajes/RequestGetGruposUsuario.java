package com.example.proyectomensajes;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class RequestGetGruposUsuario extends StringRequest {

    private static final String ruta = "http://192.168.3.81/AndroidChat/getGrupos.php";
    private Map<String, String> parametros;


    public RequestGetGruposUsuario(Integer idUser, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, ruta, listener, errorListener);
        parametros = new HashMap<>();
        parametros.put("idUser", idUser.toString());
    }

    protected Map<String, String> getParams() {
        return parametros;
    }

}
