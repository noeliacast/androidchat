package com.example.proyectomensajes;

public class Grupo {

    int id;
    String nombreGurpo;

    public Grupo(int id, String nombreGurpo) {
        this.id = id;
        this.nombreGurpo = nombreGurpo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreGurpo() {
        return nombreGurpo;
    }

    public void setNombreGurpo(String nombreGurpo) {
        this.nombreGurpo = nombreGurpo;
    }
}
