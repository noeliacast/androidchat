package com.example.proyectomensajes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BBDD extends SQLiteOpenHelper {


    public BBDD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        sqLiteDatabase.execSQL("CREATE TABLE \"Usuarios\" (\n" +
                "\t\"Id\"\tINTEGER NOT NULL PRIMARY KEY,\n" +
                "\t\"Nombre_Usuario\"\tTEXT NOT NULL UNIQUE,\n" +
                "\t\"Password\"\tTEXT NOT NULL,\n" +
                "\t\"FotoPerfil\"\tINTEGER \n" +
                ");");

        sqLiteDatabase.execSQL("CREATE TABLE \"Grupos\" (\n" +
                "\t\"Id\"\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t\"Nombre_Grupo\"\tINTEGER NOT NULL\n" +
                ");");

        sqLiteDatabase.execSQL("CREATE TABLE \"Grupos_Usuarios\" (\n" +
                "\t\"Id\"\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t\"Usuario\"\tINTEGER NOT NULL,\n" +
                "\t\"Grupo\"\tINTEGER NOT NULL,\n" +
                "\tFOREIGN KEY(\"Usuario\") REFERENCES \"Usuarios\"(\"Id\"),\n" +
                "\tFOREIGN KEY(\"Grupo\") REFERENCES \"Grupos\"(\"Id\")\n" +
                ");");

        sqLiteDatabase.execSQL("CREATE TABLE \"Chats\" (\n" +
                "\t\"Id\"\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t\"Grupo\"\tINTEGER NOT NULL,\n" +
                "\t\"Remitente\"\tINTEGER NOT NULL,\n" +
                "\t\"Mensaje\"\tTEXT NOT NULL,\n" +
                "\t\"fecha_env\"\tTEXT NOT NULL,\n" +
                "\tFOREIGN KEY(\"Grupo\") REFERENCES \"Grupos\"(\"Id\"),\n" +
                "\tFOREIGN KEY(\"Remitente\") REFERENCES \"Usuarios\"(\"Id\")\n" +
                ");");

        sqLiteDatabase.execSQL("CREATE TABLE \"MensajesEstados\" (\n" +
                "\t\"Id\"\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t\"IdMensaje\"\tINTEGER NOT NULL,\n" +
                "\t\"Recibido\"\tINTEGER,\n" +
                "\tFOREIGN KEY(\"IdMensaje\") REFERENCES \"Chats\"(\"Id\")\n" +
                ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
