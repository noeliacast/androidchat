package com.example.proyectomensajes;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class RequestEnviarMensaje extends StringRequest {
    private static String ruta = "http://192.168.3.81/AndroidChat/enviarMensajes.php";
    private Map<String, String> parametros;

    public RequestEnviarMensaje(Integer idGrupo, Integer idUsuario, String mensaje, Response.Listener<String> listener)
    {
        super(Method.POST, ruta, listener, null);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
        parametros = new HashMap<>();
        parametros.put("UserID", idUsuario.toString());
        parametros.put("Mensaje", mensaje);
        parametros.put("Grupo", idGrupo.toString());
        parametros.put("Fecha", dateFormat.format(new Date()));
    }

    protected Map<String, String> getParams() {
        return parametros;
    }
}
